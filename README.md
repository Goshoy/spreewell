# Spree Well

## Project Description

An android app for online shopping.
The app is implemented using Navigation Component, Room, 
MVVM architecture + Repository and Observer Pattern
and basic Networking.