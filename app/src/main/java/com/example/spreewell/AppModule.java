package com.example.spreewell;

import android.app.Application;

import androidx.room.Room;

import com.example.spreewell.model.db.SpreeWellDao;
import com.example.spreewell.model.db.SpreeWellDatabase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppModule {

    private static final String BASE_URL = "http://192.168.0.103:8080";
    private static Retrofit retrofit;
    private static SpreeWellDatabase spreeWellDatabase;
    private static SpreeWellDao spreeWellDao;


    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public static SpreeWellDatabase provideDatabase(Application application) {
        if (spreeWellDatabase == null) {
            spreeWellDatabase = Room.databaseBuilder(application, SpreeWellDatabase.class, "spreewell_db")
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return spreeWellDatabase;
    }

    public static SpreeWellDao provideDao(SpreeWellDatabase database) {
        if (spreeWellDao == null) {
            spreeWellDao = database.spreeWellDao();
        }
        return spreeWellDao;
    }
}