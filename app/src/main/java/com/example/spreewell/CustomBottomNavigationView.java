package com.example.spreewell;

import android.content.Context;
import android.util.AttributeSet;
import android.util.SparseArray;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;


// This class is translated from Kotlin to Android by me.
// This is a workaround until the Navigation Component supports multiple back stacks.
public class CustomBottomNavigationView extends BottomNavigationView {

    public static final String TAG = "CustomBottomNavView";

    // Map of tags
    SparseArray<String> graphIdToTagMap;
    // Result. Mutable live data with the selected controlled
    MutableLiveData<NavController> selectedNavController = new MutableLiveData<>();
    int firstFragmentGraphId;
    StateWrapper stateWrapper;

    public CustomBottomNavigationView(Context context) {
        super(context);
    }

    public CustomBottomNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LiveData<NavController> setupWithNavController(List<Integer> navGraphIds,
                                                          FragmentManager fragmentManager,
                                                          int containerId) {

        graphIdToTagMap = new SparseArray<>();
        selectedNavController = new MutableLiveData<>();
        firstFragmentGraphId = 0;

        // First create a NavHostFragment for each NavGraph ID
        createNavHostFragments(navGraphIds, fragmentManager, containerId, selectedNavController);

        // Now connect selecting an item with swapping Fragments
        initStateWrapper();

        setOnNavigationItemSelectedListener(item -> {
            // Don't do anything if the state is state has already been saved.
            if (fragmentManager.isStateSaved()) {
                return false;
            } else {
                String newlySelectedItemTag = graphIdToTagMap.get(item.getItemId());
                if (!stateWrapper.selectedItemTag.equals(newlySelectedItemTag)) {
                    // Pop everything above the first fragment (the "fixed start destination")
                    fragmentManager.popBackStack(stateWrapper.firstFragmentTag,
                            FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    NavHostFragment selectedFragment = (NavHostFragment) fragmentManager.findFragmentByTag(newlySelectedItemTag);

                    // Exclude the first fragment tag because it's always in the back stack.
                    if (!stateWrapper.firstFragmentTag.equals(newlySelectedItemTag)) {
                        // Commit a transaction that cleans the back stack and adds the first fragment
                        // to it, creating the fixed started destination.
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction()
                                .attach(selectedFragment)
                                .setPrimaryNavigationFragment(selectedFragment);

                        for (int i = 0; i < graphIdToTagMap.size(); i++) {
                            if (!graphIdToTagMap.valueAt(i).equals(newlySelectedItemTag)) {
                                fragmentTransaction.detach(fragmentManager.findFragmentByTag(stateWrapper.firstFragmentTag));
                            }
                        }

                        fragmentTransaction.addToBackStack(stateWrapper.firstFragmentTag)
                                .setCustomAnimations(
                                        R.anim.nav_default_enter_anim,
                                        R.anim.nav_default_exit_anim,
                                        R.anim.nav_default_pop_enter_anim,
                                        R.anim.nav_default_pop_exit_anim)
                                .setReorderingAllowed(true)
                                .commit();
                    }
                    stateWrapper.selectedItemTag = newlySelectedItemTag;
                    stateWrapper.isOnFirstFragment = stateWrapper.selectedItemTag.equals(stateWrapper.firstFragmentTag);
                    selectedNavController.setValue(selectedFragment.getNavController());
                    return true;
                } else {
                    return false;
                }
            }
        });

        // Finally, ensure that we update our BottomNavigationView when the back stack changes
        updateBottomNavigationViewOnBackStackChange(fragmentManager);

        return selectedNavController;
    }


    private void createNavHostFragments(List<Integer> navGraphIds, FragmentManager fragmentManager, int containerId, MutableLiveData<NavController> selectedNavController) {
        for (int index = 0; index < navGraphIds.size(); index++) {
            String fragmentTag = getFragmentTag(index);

            // Find or create the Navigation host fragment
            NavHostFragment navHostFragment = obtainNavHostFragment(
                    fragmentManager,
                    fragmentTag,
                    navGraphIds.get(index),
                    containerId
            );

            // Obtain its id
            int graphId = navHostFragment.getNavController().getGraph().getId();

            if (index == 0) {
                firstFragmentGraphId = graphId;
            }

            // Save to the map
            graphIdToTagMap.put(graphId, fragmentTag);

            // Attach or detach nav host fragment depending on whether it's the selected item.
            if (this.getSelectedItemId() == graphId) {
                // Update livedata with the selected graph
                selectedNavController.setValue(navHostFragment.getNavController());
                attachNavHostFragment(fragmentManager, navHostFragment, index == 0);
            } else {
                detachNavHostFragment(fragmentManager, navHostFragment);
            }
        }
    }

    private void updateBottomNavigationViewOnBackStackChange(FragmentManager fragmentManager) {
        final int finalFirstFragmentGraphId = firstFragmentGraphId;
        fragmentManager.addOnBackStackChangedListener(() -> {
            if (!stateWrapper.isOnFirstFragment && !isOnBackStack(stateWrapper.firstFragmentTag, fragmentManager)) {
                this.setSelectedItemId(finalFirstFragmentGraphId);
            }

            // Reset the graph if the currentDestination is not valid (happens when the back
            // stack is popped after using the back button).
            if (selectedNavController.getValue() != null) {
                if (selectedNavController.getValue().getCurrentDestination() == null) {
                    selectedNavController.getValue().navigate(selectedNavController.getValue().getGraph().getId());
                }
            }
        });
    }

    private void initStateWrapper() {
        stateWrapper = new StateWrapper();
        stateWrapper.selectedItemTag = graphIdToTagMap.get(this.getSelectedItemId());
        stateWrapper.firstFragmentTag = graphIdToTagMap.get(firstFragmentGraphId);
        stateWrapper.isOnFirstFragment = stateWrapper.selectedItemTag.equals(stateWrapper.firstFragmentTag);
    }

    private void detachNavHostFragment(
            FragmentManager fragmentManager,
            NavHostFragment navHostFragment
    ) {
        fragmentManager.beginTransaction()
                .detach(navHostFragment)
                .commitNow();
    }

    private void attachNavHostFragment(
            FragmentManager fragmentManager,
            NavHostFragment navHostFragment,
            Boolean isPrimaryNavFragment
    ) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction()
                .attach(navHostFragment);
        if (isPrimaryNavFragment) {
            fragmentTransaction.setPrimaryNavigationFragment(navHostFragment);
        }
        fragmentTransaction.commitNow();
    }

    private NavHostFragment obtainNavHostFragment(
            FragmentManager fragmentManager,
            String fragmentTag,
            int navGraphId,
            int containerId) {
        // If the Nav Host fragment exists, return it
        NavHostFragment existingFragment = (NavHostFragment) fragmentManager.findFragmentByTag(fragmentTag);
        if (existingFragment != null) {
            return existingFragment;
        }
        // Otherwise, create it and return it.
        NavHostFragment navHostFragment = NavHostFragment.create(navGraphId);
        fragmentManager.beginTransaction()
                .add(containerId, navHostFragment, fragmentTag)
                .commitNow();
        return navHostFragment;
    }

    private boolean isOnBackStack(String backStackName, FragmentManager fragmentManager) {
        int backStackCount = fragmentManager.getBackStackEntryCount();
        for (int i = 0; i < backStackCount; i++) {
            if (fragmentManager.getBackStackEntryAt(i).getName().equals(backStackName)) {
                return true;
            }
        }
        return false;
    }

    private String getFragmentTag(int index) {
        return "bottomNavigation#" + index;
    }

    class StateWrapper {
        String selectedItemTag;
        String firstFragmentTag;
        Boolean isOnFirstFragment;
    }
}
