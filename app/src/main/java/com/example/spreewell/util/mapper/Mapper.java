package com.example.spreewell.util.mapper;

public interface Mapper<I, O> {

    public O map(I input);
}
