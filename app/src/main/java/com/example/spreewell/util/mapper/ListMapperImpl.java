package com.example.spreewell.util.mapper;

import java.util.ArrayList;
import java.util.List;

public class ListMapperImpl<I, O> implements ListMapper<I, O> {

    private Mapper<I, O> mapper;

    public ListMapperImpl(Mapper<I, O> mapper) {
        this.mapper = mapper;
    }

    @Override
    public List<O> map(List<I> input) {
        List<O> result = new ArrayList<>(input.size());
        for (I inputValue : input) {
            result.add(mapper.map(inputValue));
        }
        return result;
    }
}
