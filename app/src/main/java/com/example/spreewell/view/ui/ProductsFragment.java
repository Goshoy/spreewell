package com.example.spreewell.view.ui;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.spreewell.AppModule;
import com.example.spreewell.R;
import com.example.spreewell.model.Product;
import com.example.spreewell.model.repository.ProductRepository;
import com.example.spreewell.view.adapter.ProductCardAdapter;
import com.example.spreewell.viewmodel.ProductViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductsFragment extends Fragment {

    private static final String TAG = "ProductsFragment";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ProductViewModel productViewModel = new ProductViewModel(new ProductRepository((AppModule.provideDao(AppModule.provideDatabase(getActivity().getApplication())))));
        LiveData<List<Product>> productsLiveData = productViewModel.getProducts();
        observeProductsLiveData(view, productsLiveData);
    }

    private void observeProductsLiveData(@NonNull View view, LiveData<List<Product>> productsLiveData) {
        productsLiveData.observe(this, productsResource -> {
            if (productsResource != null && productsResource.size() > 0) {
                Map<String, List<Product>> productsByCategories = new HashMap<>();

                for (Product product : productsResource) {
                    String category = product.getCategory().getTitle();
                    if (!productsByCategories.containsKey(category)) {
                        productsByCategories.put(category, new ArrayList<>());
                    }
                    productsByCategories.get(category).add(product);
                }

                populateUI(view, productsByCategories);
            }
        });
    }

    public void populateUI(View view, Map<String, List<Product>> productsByCategories) {
        LinearLayout linearLayout = getView().findViewById(R.id.listProductsLinearLayout);
        linearLayout.removeAllViews();

        if (linearLayout != null) {
            ViewGroup.LayoutParams params = linearLayout.getLayoutParams();
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(params);
            layoutParams.setMargins(15, 15, 0, 0);

            for (Map.Entry<String, List<Product>> products : productsByCategories.entrySet()) {
                addCategoryTextView(view, linearLayout, params, products.getKey());
                addHorizontalRecyclerViewWithProducts(view, linearLayout, layoutParams, products.getValue());
            }
        }
    }

    private void addHorizontalRecyclerViewWithProducts(View view, LinearLayout linearLayout,
                                                       LinearLayout.LayoutParams layoutParams, List<Product> products) {
        RecyclerView recyclerView = new RecyclerView(view.getContext());
        recyclerView.setLayoutParams(layoutParams);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false));
        ProductCardAdapter adapter = new ProductCardAdapter(products);
        adapter.setOnItemClickListener(new ProductCardAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Product product) {

                ProductsFragmentDirections.ActionProductsFragmentToProductInfoFragment action =
                        ProductsFragmentDirections.actionProductsFragmentToProductInfoFragment();
                action.setClickedProduct(product);
                Navigation.findNavController(view).navigate(action);
            }
        });
        recyclerView.setAdapter(adapter);

        linearLayout.addView(recyclerView);
    }

    private void addCategoryTextView(View view, LinearLayout linearLayout, ViewGroup.LayoutParams params, String category) {
        TextView text = new TextView(view.getContext());
        text.setLayoutParams(params);
        text.setHeight(100);
        text.setPadding(60, 0, 10, 0);
        text.setTypeface(null, Typeface.BOLD);
        text.setText(category);
        text.setTextSize(25);

        linearLayout.addView(text);
    }
}
