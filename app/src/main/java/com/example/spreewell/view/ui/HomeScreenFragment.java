package com.example.spreewell.view.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.spreewell.CustomBottomNavigationView;
import com.example.spreewell.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;

import java.util.Arrays;
import java.util.List;

public class HomeScreenFragment extends Fragment {
    private static final String TAG = "HomeScreenFragment";

    private CustomBottomNavigationView bottomNavigationView;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;

    private GoogleSignInClient mGoogleSignInClient;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpGoogleSingInClient();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_screen, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        drawerLayout = view.findViewById(R.id.drawerLayout);
        toolbar = view.findViewById(R.id.appToolbar);

        setUpNavigationView(view);

        if (savedInstanceState == null) {
            setupBottomNavigationBar();
        }
    }

    private void setUpNavigationView(@NonNull View view) {
        NavigationView navigationView = view.findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.logout:
                    logOut();
                    break;
            }
            return false;
        });
    }

    @SuppressLint("ResourceType")
    private void setupBottomNavigationBar() {
        bottomNavigationView = getView().findViewById(R.id.bottomNavigation);
        List<Integer> navGraphIds = Arrays.asList(R.navigation.products, R.navigation.cart, R.navigation.map);

        LiveData<NavController> controller = bottomNavigationView.setupWithNavController(
                navGraphIds,
                getChildFragmentManager(),
                R.id.nav_host_container
        );

        controller.observe(this, navController -> {

            AppBarConfiguration appBarConfiguration =
                    new AppBarConfiguration.Builder(navController.getGraph()).setDrawerLayout(drawerLayout).build();
            NavigationUI.setupWithNavController(toolbar, navController, appBarConfiguration);
        });
    }

    private void setUpGoogleSingInClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
    }

    void logOut() {
        Activity that = getActivity();
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(that, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        NavController controller = Navigation.findNavController(that, R.id.nav_host_fragment);
                        Log.d(TAG, "debug, logOut current dest label = " + controller.getCurrentDestination().getLabel());
                        Navigation.findNavController(that, R.id.nav_host_fragment).popBackStack();
                        Log.d(TAG, "debug, logOut current dest label = " + controller.getCurrentDestination().getLabel());
                    }
                });
    }
}
