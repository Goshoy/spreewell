package com.example.spreewell.view.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.spreewell.R;
import com.example.spreewell.model.Product;

public class ProductInfoFragment extends Fragment {

    private Product clickedProduct;

    private ImageView picture;
    private TextView title;
    private TextView description;

    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_info, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        clickedProduct = ProductInfoFragmentArgs.fromBundle(getArguments()).getClickedProduct();
        picture = view.findViewById(R.id.productImage);
        title = view.findViewById(R.id.productTitle);
        description = view.findViewById(R.id.productDescription);

        setProductInfo();
    }

    public void setProductInfo() {
        byte[] pictureBytes = Base64.decode(clickedProduct.getImage(), Base64.DEFAULT);
        Bitmap bmp = BitmapFactory.decodeByteArray(pictureBytes, 0, pictureBytes.length);

        this.picture.setImageBitmap(bmp);
        this.title.setText(clickedProduct.getTitle());
        this.description.setText(clickedProduct.getDescription());
    }
}
