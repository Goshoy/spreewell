package com.example.spreewell.model.network.model;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}
