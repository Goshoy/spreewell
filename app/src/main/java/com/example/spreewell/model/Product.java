package com.example.spreewell.model;

import java.io.Serializable;

public class Product implements Serializable {

    private String title;
    private String image;
    private String price;
    private String description;
    private Category category;

    public Product() {
    }

    public Product(String title, String image, String price, String description, String category) {
        this.title = title;
        this.image = image;
        this.price = price;
        this.description = description;
        this.category = new Category(category);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public class Category {

        private String title;

        public Category(String title) {
            this.title = title;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
