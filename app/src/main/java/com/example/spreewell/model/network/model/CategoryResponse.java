package com.example.spreewell.model.network.model;

import java.util.ArrayList;
import java.util.List;

public class CategoryResponse {
    private List<NetworkCategory> categories = new ArrayList<>();

    public CategoryResponse() {
    }

    public List<NetworkCategory> getCategories() {
        return categories;
    }
}
