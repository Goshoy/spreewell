package com.example.spreewell.model.network;


import com.example.spreewell.model.network.model.CategoryResponse;
import com.example.spreewell.model.network.model.ProductResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface WebService {

    @GET("/products")
    Call<ProductResponse> getProducts();

    @GET("/categories")
    Call<CategoryResponse> getCategories();
}
