package com.example.spreewell.model.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.spreewell.model.db.entity.CategoryEntity;
import com.example.spreewell.model.db.entity.ProductEntity;

@Database(entities = {ProductEntity.class, CategoryEntity.class}, version = 8, exportSchema = false)
public abstract class SpreeWellDatabase extends RoomDatabase {

    public abstract SpreeWellDao spreeWellDao();
}
