package com.example.spreewell.model.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.spreewell.model.db.entity.CategoryEntity;
import com.example.spreewell.model.db.entity.ProductEntity;
import com.example.spreewell.model.db.entity.ProductWrapper;

import java.util.List;

@Dao
public interface SpreeWellDao {
    @Query("SELECT * FROM category")
    LiveData<List<CategoryEntity>> getCategories();

    @Query("SELECT id FROM CATEGORY WHERE title =:title")
    int getCategoryId(String title);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveCategory(CategoryEntity categoryEntity);

    @Query("DELETE FROM category")
    void deleteCategories();

    @Query("SELECT * FROM product")
    LiveData<List<ProductEntity>> getProducts();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveProduct(ProductEntity productEntity);

    @Query("DELETE FROM product")
    void deleteProducts();

    @Query("SELECT * FROM product")
    LiveData<List<ProductWrapper>> getProductWrappers();

}
