package com.example.spreewell.model.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.example.spreewell.AppModule;
import com.example.spreewell.model.Product;
import com.example.spreewell.model.db.SpreeWellDao;
import com.example.spreewell.model.db.entity.CategoryEntity;
import com.example.spreewell.model.db.entity.ProductEntity;
import com.example.spreewell.model.db.entity.ProductWrapper;
import com.example.spreewell.model.network.NetworkBoundResource;
import com.example.spreewell.model.network.WebService;
import com.example.spreewell.model.network.model.NetworkProduct;
import com.example.spreewell.model.network.model.ProductResponse;
import com.example.spreewell.model.network.model.Resource;
import com.example.spreewell.util.AppExecutors;
import com.example.spreewell.util.mapper.ListMapperImpl;
import com.example.spreewell.util.mapper.Mapper;

import java.util.List;

import retrofit2.Call;

public class ProductRepository {
    private final String TAG = ProductRepository.class.getSimpleName();
    private final WebService webService;
    private final SpreeWellDao spreeWellDao;
    private final Mapper<ProductWrapper, Product> productEntityMapper;

    public ProductRepository(SpreeWellDao spreeWellDao) {
        webService = AppModule.getRetrofitInstance().create(WebService.class);
        this.spreeWellDao = spreeWellDao;
        productEntityMapper = new Mapper<ProductWrapper, Product>() {
            @Override
            public Product map(ProductWrapper input) {
                ProductEntity productEntity = input.getProductEntity();
                CategoryEntity categoryEntity = input.getCategoryEntity().get(0);
                Product product = new Product(productEntity.getTitle(), productEntity.getPicture(),
                        productEntity.getPrice(), productEntity.getDescription(), categoryEntity.getTitle());
                return product;
            }
        };
    }

    public LiveData<List<Product>> getProducts() {
        LiveData<Resource<List<ProductWrapper>>> data = new NetworkBoundResource<List<ProductWrapper>, ProductResponse>(new AppExecutors()) {
            @Override
            protected void saveCallResult(@NonNull ProductResponse item) {
                if (item != null) {
                    for (NetworkProduct product : item.getProducts()) {
                        String categoryTitle = product.getCategory().getTitle();
                        if (spreeWellDao.getCategoryId(categoryTitle) == 0) {
                            CategoryEntity categoryEntity = new CategoryEntity(categoryTitle);
                            spreeWellDao.saveCategory(categoryEntity);
                        }

                        int categoryId = spreeWellDao.getCategoryId(product.getCategory().getTitle());
                        ProductEntity productEntity = new ProductEntity(categoryId, product.getTitle(), product.getPicture(),
                                product.getDescription(), product.getPrice());
                        spreeWellDao.saveProduct(productEntity);
                    }
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<ProductWrapper> data) {
                return data == null || data.size() == 0;
            }

            @NonNull
            @Override
            protected LiveData<List<ProductWrapper>> loadFromDb() {
                return spreeWellDao.getProductWrappers();
            }

            @NonNull
            @Override
            protected Call<ProductResponse> createCall() {
                return webService.getProducts();
            }
        }.asLiveData();

        MediatorLiveData<List<Product>> mediatorLiveData = new MediatorLiveData<>();
        mediatorLiveData.addSource(data, listResource -> {
            List<Product> products = null;
            if (listResource.data != null) {
                //Map ProductWrapper to Product.
                ListMapperImpl<ProductWrapper, Product> listMapper = new ListMapperImpl<>(productEntityMapper);
                products = listMapper.map(listResource.data);
            }

            mediatorLiveData.setValue(products);
        });

        return mediatorLiveData;
    }

}
