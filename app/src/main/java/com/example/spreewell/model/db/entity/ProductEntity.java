package com.example.spreewell.model.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "product", foreignKeys = @ForeignKey(entity = CategoryEntity.class, parentColumns = "id", childColumns = "categoryId", onDelete = CASCADE))
public class ProductEntity {

    @PrimaryKey(autoGenerate = true)
    public int id;
    public int categoryId;
    @ColumnInfo(name = "title")
    public String title;
    @ColumnInfo(name = "picture")
    public String picture;
    @ColumnInfo(name = "description")
    public String description;
    @ColumnInfo(name = "price")
    public String price;

    public ProductEntity(int categoryId, String title, String picture, String description, String price) {
        this.categoryId = categoryId;
        this.title = title;
        this.picture = picture;
        this.description = description;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
