package com.example.spreewell.model.network.model;

import java.util.ArrayList;
import java.util.List;

public class ProductResponse {
    List<NetworkProduct> products = new ArrayList<>();

    public List<NetworkProduct> getProducts() {
        return products;
    }
}
