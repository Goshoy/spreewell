package com.example.spreewell.model.db.entity;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class ProductWrapper {
    @Embedded
    ProductEntity productEntity;

    @Relation(parentColumn = "categoryId", entityColumn = "id")
    List<CategoryEntity> categoryEntity;

    public ProductWrapper() {
    }

    public ProductWrapper(ProductEntity productEntity, List<CategoryEntity> categoryEntity) {
        this.productEntity = productEntity;
        this.categoryEntity = categoryEntity;
    }

    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public List<CategoryEntity> getCategoryEntity() {
        return categoryEntity;
    }

    public void setCategoryEntity(List<CategoryEntity> categoryEntity) {
        this.categoryEntity = categoryEntity;
    }
}
