package com.example.spreewell.model.network.model;

import com.google.gson.annotations.SerializedName;

public class NetworkCategory {

    @SerializedName("title")
    private String title;

    public NetworkCategory(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
