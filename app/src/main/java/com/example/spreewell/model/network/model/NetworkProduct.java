package com.example.spreewell.model.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.List;

public class NetworkProduct {
    @SerializedName("title")
    private String title;
    @SerializedName("price")
    private String price;
    @SerializedName("description")
    private String description;
    @SerializedName("picture")
    private String picture;
    @SerializedName("category")
    private NetworkCategory category;

    public NetworkProduct(String title, String price, String info) {
        this.title = title;
        this.price = price;
    }

    // A workaround before the server is ready to send products!
    public static List<NetworkProduct> init() {
        String productTitle = "Micellar Water";
        String productPrice = "59.99";
        String productDescription = "Very powerful product that clears everything with ease." +
                "It is on the base of water and can`t do any harm.All experts are suggesting it.";

        NetworkProduct product = new NetworkProduct(productTitle, productPrice, productDescription);

        return Arrays.asList(product, product, product, product);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public NetworkCategory getCategory() {
        return category;
    }

    public void setCategory(NetworkCategory category) {
        this.category = category;
    }
}
